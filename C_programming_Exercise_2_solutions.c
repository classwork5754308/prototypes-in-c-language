#include <stdio.h>
#include <stdlib.h>


struct Node
{
    int number;
    struct Node *next;
};

// Function to create a new node
struct Node* createNode(int num) {
    // Allocate memory for the new node
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));

    // Check if memory allocation is successful
    if (newNode == NULL) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }

    // Initialize the data and next fields
    newNode->number = num;
    newNode->next = NULL; // If it's a linked list node, initialize next to NULL

    return newNode;
}

// Function to print the linked list
void printList(struct Node* head) {
    struct Node* current = head;

    // Traverse the list and print each element
    while (current != NULL) {
        printf("%d -> ", current->number);
        current = current->next;
    }

    printf("NULL\n");
}
//Create a New Node: First, create a new node to hold the number you want to append. Allocate memory for the new node using malloc and set its data value to the desired number.
//Traverse to the End: Traverse the linked list from the head to find the last node. Keep moving to the next node until you reach the end (i.e., the node whose next pointer is NULL).
//Update Pointers: Once you reach the end, update the next pointer of the last node to point to the newly created node. Also, set the next pointer of the new node to NULL.
// Function to append a number to a linked list
struct Node* append(struct Node* head, int num) {
    // Create a new node
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));

    // Check if memory allocation is successful
    if (newNode == NULL) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }

    // Assign the value to the new node
    newNode->number = num;
    newNode->next = NULL; // The new node will be the last node, so its next should be NULL

    // If the linked list is empty, make the new node the head
    if (head == NULL) {
        head = newNode;
    } else {
        // Traverse the list to find the last node
        struct Node* current = head;
        while (current->next != NULL) {
            current = current->next;
        }

        // Insert the new node at the end
        current->next = newNode;
    }

    return head;
}
//Allocate memory for the new node.
//Set the new node’s data to the desired value.
//Update the new node’s next pointer to point to the current head of the list.
//Update the head pointer to point to the new node.
// Function to prepend a number to a linked list
void prepend(struct Node* head, int num) {
    // Create a new node
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));

    // Check if memory allocation is successful
    if (newNode == NULL) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }

    // Assign the value to the new node
    newNode->number = num;

    // Make the new node the new head of the linked list
    newNode->next = head;
    head = newNode;

    return head;
}
// Function to delete a node by key
//Find the previous node of the node to be deleted.
//Change the next pointer of the previous node to skip the node to be deleted.
//Free the memory allocated for the node to be deleted.
void deleteNodeByKey(struct Node** head, int key) {
    // Check if the list is empty
    if (*head == NULL) {
        printf("List is empty, cannot delete.\n");
        return;
    }

    // Initialize pointers for traversal
    struct Node* current = *head;
    struct Node* previous = NULL;

    // Traverse the list to find the node with the specified key
    while (current != NULL && current->number != key) {
        previous = current;
        current = current->next;
    }

    // Check if the key was not found
    if (current == NULL) {
        printf("Key not found, cannot delete.\n");
        return;
    }

    // If the node to be deleted is the head
    if (previous == NULL) {
        *head = current->next;
    } else {
        // Otherwise, update the previous node's next pointer
        previous->next = current->next;
    }

    // Free the memory of the node to be deleted
    free(current);
    printf("Node with key %d deleted successfully.\n", key);
}
// Function to delete a node by value
//Find the previous node of the node to be deleted.
//Change the next pointer of the previous node to skip the node to be deleted.
//Free the memory allocated for the node to be deleted.
void deleteNodeByValue(struct Node** head, int value) {
    // Initialize pointers for traversal and a pointer to the previous node
    struct Node* current = *head;
    struct Node* prev = NULL;

    // Traverse the list to find the node with the specified value
    while (current != NULL && current->number != value) {
        prev = current;
        current = current->next;
    }

    // If the node with the specified value is not found
    if (current == NULL) {
        printf("Node with value %d not found\n", value);
        return;
    }

    // Adjust pointers to skip the node
    if (prev == NULL) {
        // If the node to be deleted is the head
        *head = current->next;
    } else {
        prev->next = current->next;
    }

    // Free the memory of the deleted node
    free(current);
}
// Function to insert a new node after a specified key
//Search for the key in the linked list.
//If the key is found:
//Create a new node with the desired value.
//Set the new node’s next pointer to the next node after the key.
//Update the next pointer of the key node to point to the new node.
void insertAfterKey(struct Node** head, int key, int value) {
    struct Node* current = head;

    // Find the key in the linked list
    while (current != NULL && current->number != key) {
        current = current->next;
    }

    // If key is not found
    if (current == NULL) {
        printf("Key not found in the linked list.\n");
        return;
    }

    // Create a new node for the new element
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    newNode->number = value;

    // Insert the new node after the key
    newNode->next = current->next;
    current->next = newNode;
}
// Function to insert a new node after a specified value
//Search for the value in the linked list.
//If the value is found:
//Create a new node with the desired value.
//Set the new node’s next pointer to the next node after the value.
//Update the next pointer of the value node to point to the new node.
void insertAfterValue(struct Node** head, int searchValue, int newValue) {
    struct Node* current = head;

    // Find the value in the linked list
    while (current != NULL && current->number != searchValue) {
        current = current->next;
    }

    // If value is not found
    if (current == NULL) {
        printf("Value not found in the linked list.\n");
        return;
    }

    // Create a new node for the new element
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    newNode->number = newValue;

    // Insert the new node after the value
    newNode->next = current->next;
    current->next = newNode;
}
int main() {
    struct Node* head = NULL;
    int choice, data;

    while (1) {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                head = append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Enter key to delete: ");
                scanf("%d", &data);
                deleteNodeByKey(&head, data);
                break;
            case 5:
                printf("Enter value to delete: ");
                scanf("%d", &data);
                deleteNodeByValue(&head, data);
                break;
            case 6:
                printf("Enter key after which to insert: ");
                scanf("%d", &data);
                printf("Enter value to insert: ");
                int newValue;
                scanf("%d", &newValue);
                insertAfterKey(&head, data, newValue);
                break;
            case 7:
                printf("Enter value after which to insert: ");
                scanf("%d", &data);
                printf("Enter value to insert: ");
                scanf("%d", &newValue);
                insertAfterValue(&head, data, newValue);
                break;
            case 8:
                // Exit the program
                printf("Exiting the program.\n");
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
